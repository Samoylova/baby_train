#!/usr/bin/env python2.7
import socket
import sqlite3
import random
import hashlib
import string
from sys import argv
import os

LOG = 1
PORT = 31337

DBNAME = "baby_notes.db"

CODES = {'OK': 101, 'CORRUPT': 102, 'MUMBLE': 103, 'DOWN': 104, 'CHECKER_ERROR': 110}

SHIT = {'conn':          ("Unable to connect to the service", CODES['DOWN']),
        'prot':          ("Server doenst answers properly", CODES['MUMBLE']),
        'allok':         ("Everything is fine", CODES['OK']),
        'flggg':         ("Wrong flag", CODES['CORRUPT']),
        'wtf':           ("Man, you should correct your checker", CODES['CHECKER_ERROR']),
       }

def check(hostname):
    flag = _gen_secret()

    s = _connect(hostname)
    _recv_until(s, 'Exit')             # Welcome

    login = _gen_random_string()

    password = _put(s, login, flag)

    s.send(b'5\n') #Exit
    s.close()
    s = _connect(hostname)
    _recv_until(s, 'Exit')

    _get(s,login, password, flag)

    s.send(b'5\n') #Exit
    s.close()

    _die('allok')

def put(hostname, fid, flag):
    s = _connect(hostname)
    _recv_until(s, 'Exit')

    password = _put(s, fid, flag)
    _log('Get flag id: "{}"'.format(password))
    _save_id(fid, password)

    s.send(b'5\n') #Exit
    s.close()

    _die('allok')

def get(hostname, fid, flag):
    s = _connect(hostname)
    _recv_until(s, 'Exit')             # Welcome

    password = _get_pass(fid)
    _get(s, fid, password, flag)

    s.send(b'3\n')                      # Exit
    s.close()

    _die('allok')
def _get(s, fid, password, flag):
    s.send(b'2\n')
    answ = _recv_until(s, ':')
    if 'log' not in answ:
        _die('prot')

    s.send((fid+"\n").encode())
    answ = _recv_until(s, ':')
    if 'pass' not in answ:
        _die('prot')

    s.send((password+"\n").encode())
    answ = _recv_until(s, 'Exit')
    if 'OK' not in answ:
        _die('flggg')

    s.send(b'3\n')
    answ = _recv_until(s, 'Exit')
    if flag not in answ:
        _die('flggg')

def _get_pass(fid):
    db = get_db()
    c = db.cursor()
    query = '''
                SELECT password FROM ids
                WHERE flagid = ?
                '''
    c.execute(query, (fid,))
    password = c.fetchone()[0]
    return password

def _put(s, fid, flag):
    s.send(b'1\n')                      # login
    answ = _recv_until(s, ':')
    if 'log' not in answ:
        _die('prot')

    s.send((fid + '\n').encode())
    answ = _recv_until(s, ':')
    if 'pass' not in answ:
        _die('prot')
    
    password = _gen_random_string()
    s.send((password + '\n').encode())
    answ = _recv_until(s, 'Exit')
    if 'OK' not in answ:
        _die('prot')

    s.send(b'4\n')
    answ = _recv_until(s,':')
    if 'Input' not in answ:
        _die('prot')
    s.send((flag + '\n').encode())
    answ = _recv_until(s, 'Exit')
    if 'OK' not in answ:
        _die('prot')

    return password


def _save_id(flag_id, given_id):
    db = get_db()
    c = db.cursor()
    query = '''
            INSERT INTO ids(flagid, password)
            VALUES (?,?)
            '''
    c.execute(query, (flag_id, given_id,))
    db.commit()

def _connect(hostname):
    try:
        s = socket.create_connection((hostname, PORT))
    except Exception as e:
        _log(e)
        _die('conn')
    return s

def _log(t):
    if LOG:
        print(t)
    return t

def _recv_until(s, target):
    ans = ''
    while target not in ans:
        ans += s.recv(100500).decode()
    return _log(ans)

def _die(event):
    assert event in SHIT
    msg, code = SHIT[event]
    _log(msg)
    exit(code)

def get_db():
    return sqlite3.connect(DBNAME)

def prepare_db():
    db = get_db()
    c = db.cursor()
    cmd = '''
            CREATE TABLE IF NOT EXISTS
            ids(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                flagid TEXT,
                password TEXT
            )
        '''
    c.execute(cmd)
    db.commit()

def _gen_secret():
    templates = ["I hide my {} in my {}", "I love to put {} into {}", "Do you also like {} when you are in {}"]
    places = ["hole", "secret place", "bed", "kitchen", "work", "garden"]
    items = ["fish", "dog", "secrets", "candies", "cookie", "talala"]
    template = random.choice(templates)
    return template.format(
        random.choice(items),
        random.choice(places)
    )

def _gen_random_string():
    return os.urandom(10).encode('hex')

def main():
    prepare_db()
    get_db()
    cmd = argv[1]
    hostname = argv[2]
    if cmd == 'get':
        fid = argv[3]
        flag = argv[4]
        return get(hostname, fid, flag)
    elif cmd == 'put':
        fid = argv[3]
        flag = argv[4]
        return put(hostname, fid, flag)
    elif cmd == 'check':
        return check(hostname)
    else:
        return die('wtf')
#    except:
#        print("Usage: %s check|put|get hostname IP FLAGID FLAG " % argv[0])
#        _die('conn')


if __name__=="__main__":
    main()
