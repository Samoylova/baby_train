#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define disable_buffering(_fd) setvbuf(_fd, NULL, _IONBF, 0)
#define strip_linefeed(string) { char* nl = strchr(string, '\n'); if (nl != 0) { *nl = 0; } }

int is_login = 0;
char login[256] = {0};

char UsefulString[25] = "/bin/cat ./users/*/*\0";

int read_answ(int fd, char* buf, int size) {
    int i;
    for(i = 0; i < size && read(fd, buf, 1) == 1 && *buf != '\n'; i++, buf++);
    *buf = 0;
    return i;

}

void getPathToPass(char* path, char* login) {
  *path = '\0';
  char cwd[100] = "\0";
  getcwd(cwd, sizeof(cwd));
  strcat(path, cwd);
  strcat(path, "/users/");
  strcat(path, login);
  strcat(path, "/password");
}

void getPathToFlags(char * path, char* login) {
  *path = '\0';
  char cwd[100] = "\0";
  getcwd(cwd, sizeof(cwd));
  strcat(path, cwd);
  strcat(path, "/users/");
  strcat(path, login);
  strcat(path, "/flags");
}

void registr() {
  char pass[256];
  char cwd[70];
  getcwd(cwd, sizeof(cwd));
  if(is_login) {
    printf("You are already login as %s!\n", login);
    return;
  }
  printf("log:\n");
  read_answ(0, login, 255);
  printf("pass: \n");
  read_answ(0, pass, 255);
  printf("%s\n", pass);

  char pathToPass[512];
  char cmd[512] = "mkdir ";
  strcat(cmd, cwd);
  strcat(cmd, "/users/");
  strcat(cmd, login);

  if(system(cmd)) {
    printf("\nuser is exixst\n");
    return;
  };
  getPathToPass(pathToPass, login);

  FILE *fp = fopen(pathToPass, "w");
  if(fp == NULL) {
    printf("Something wrong...\nexiting...\n");
    exit(0);
  }
  fprintf(fp, "%s\n", pass);
  fclose(fp);

  is_login = 1;
  printf("OK\n");
}

void logIn() {
  char pass[256];
  if(is_login) {
    printf("You are already login as ");
    printf(login);
    printf("!\n");
    return;
  }
  printf("log:\n");
  read_answ(0, login, 255);
  printf("pass: \n");
  read_answ(0, pass, 255);

  char pathToPass[512] = {0};
  getPathToPass(pathToPass, login);

  FILE *fp = fopen(pathToPass, "r");
  if(fp == NULL) {
    printf("Passsword is wrong!\n");
    return;
  }
  char real_pass[256];
  fscanf(fp, "%s", real_pass);
  fclose(fp);

  if(strcmp(pass, real_pass)) {
    printf("Password is wrong!\n");
    return;
  }
  is_login = 1;
  printf("OK");
  return;
};

void get_notes() {
  if(!is_login) {
    printf("Please, login\n");
    return;
  }
  char pathToFlags[512];
  getPathToFlags(pathToFlags, login);

  char cmd[512] = "cat ";
  strcat(cmd, pathToFlags);

  printf("Your notes:\n");
  system(cmd);
};

void store_note() {
  char note[33];
  char cmd[1024] = "echo \"";
  char pathToFlags[512];

  if(!is_login) {
    printf("Please, login\n");
    return;
  }

  printf("Input your note:\n");

  read_answ(0, note, 100500);

  getPathToFlags(pathToFlags, login);

  strcat(cmd, note);
  strcat(cmd, "\" >> ");
  strcat(cmd, pathToFlags);
  system(cmd);

  puts("OK");
};

void menu() {
  char buf[256];
  while(1) {
    printf("\nMenu:\n     1.Registr\n     2.Login\n     3.Get notes\n     4.Store note\n     5.Exit\n");
    read_answ(0, buf, 4);
    switch (atoi(buf)) {
      case 1:
        registr();
        break;
      case 2:
        logIn();
        break;
      case 3:
        get_notes();
        break;
      case 4:
        store_note();
        break;
      case 5:
        exit(0);
      default:
        puts("Wrong value!");
    }

  }
}

int main() {
  disable_buffering(stdin);
  disable_buffering(stdout);
  disable_buffering(stderr);
  menu();
}
